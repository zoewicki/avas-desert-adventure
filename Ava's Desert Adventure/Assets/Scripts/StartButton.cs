﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {

    public Button ButtonPlay;
    public string LevelToLoad = "GameScene";

    private void Start()
    {
        Button btn = ButtonPlay.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene(LevelToLoad);
        Debug.Log("You have clicked the button!");
    }
}
