﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SnakeEnemy : Enemy {
    private bool bShouldAttack = false;
    public string ScreenToLoad = "Level2";
    public override void Move()
    {
        Debug.Log(bShouldRetreat + " - Should Retreat");

        if(!bShouldRetreat && !bShouldAttack)
        {
            if (Mathf.Abs(transform.position.x - Player.transform.position.x) < 6)
                bShouldAttack = true;
            else if (Mathf.Abs(transform.position.x - Player.transform.position.x) < 7)
                MyRigidbody.velocity = new Vector2(-MoveDirection() * MoveSpeed, MyRigidbody.velocity.y);
            else if (Mathf.Abs(transform.position.x - Player.transform.position.x) >= 7)
                MyRigidbody.velocity = new Vector2(MoveDirection() * MoveSpeed, MyRigidbody.velocity.y);
        }
        else if (bShouldAttack)
        {
            MyRigidbody.velocity = new Vector2(MoveDirection() * MoveSpeed * 2, MyRigidbody.velocity.y);
        }
        else if (bShouldRetreat)
        {
            MyRigidbody.velocity = new Vector2(-MoveDirection() * RetreatSpeed, MyRigidbody.velocity.y);
        }
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.collider == Player.GetComponent<Collider2D>())
        {
            bShouldRetreat = true;
            bShouldAttack = false;
            TimeSinceLastRetreat = 0f;
        }
    }

    public override void Update()
    {
        base.Update();
        if (bShouldRetreat)
        {
            TimeSinceLastRetreat += Time.deltaTime;
            if (TimeSinceLastRetreat >= RetreatTime)
            {
                bShouldRetreat = false;
                bShouldAttack = false;
                TimeSinceLastRetreat = 0f;
                Debug.Log(bShouldRetreat);
            }
        }
        if(Health <= 0)
        {
            StartCoroutine(WinLevelAfterDelay(2));
            if (Player.bTimeIsSlow)
                Player.SlowTime(Player.SlowPrecent);
            GetComponent<Renderer>().enabled = false;
        }
    }
    public override void GetHurt(float Damage)
    {
        base.GetHurt(Damage);
        if (Damage > 0)
        {
            bShouldRetreat = true;
            bShouldAttack = false;
        }
    }
    IEnumerator WinLevelAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("Loading NextScene");
        SceneManager.LoadScene(ScreenToLoad);

    }
}
