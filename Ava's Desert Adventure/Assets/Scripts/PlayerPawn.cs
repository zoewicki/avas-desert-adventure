﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : MonoBehaviour {

    public bool bIsDead = false;                    //False if the player is alive
    public float Health = 3f;                       //Number of health the player has
    public float StartHealth = 3f;                  //Number of health the player starts with
    private bool bHasBeenHurtRecently = false;      //False if the player has not been hurt recently
    public float PainDelay = 0.25f;                 //How long before the player can be hurt again
    private float TimeSinceFirstJump = 0f;          //
    private float TimeSinceLastPain = 0f;           //Time since the player was last hurt
    private const string SpeedParam = "Speed";      //
    private const string GroundParam = "Ground";    //
    private const float GroundedRadius = .2f;       // Radius of the overlap circle to determine if grounded

    public float SlowPrecent = .33f;                // The speed at which the game will run at when slowing down
    public bool bTimeIsSlow = false;               // false if time is normal speed
    [SerializeField]
    private float maxSpeed = 10f;                   // The fastest the player can travel in the x axis
    public float impactForce = 0f;                  //
    [SerializeField]
    private float jumpForce = 400f;                 // The force at which the player jumps
    public int MaxJumpCount = 2;                    // Max number of jumps a player can do. N-1 is number of mid-air jumps.
    public int CurrentJumpCount = 0;                // Current number of jumps the player has done
    [SerializeField]
    private bool airControl;                        // Whether or not a player can steer while jumping
    [SerializeField]
    private Transform groundCheck;                  // 
    [SerializeField]
    private LayerMask groundLayers;                 //

    private bool grounded;                          // Is the player on the ground?
    private bool facingRight = true;                // Is the player facing right?

    private Animator anim;
    private Rigidbody2D rb2D;

    private void Awake()
    {
        anim = GetComponent<Animator>();           // Set the animator on game start
        rb2D = GetComponent<Rigidbody2D>();        // Set the rigid body on game start
    
        if (!groundCheck)
        {
            Debug.LogError("Ground Check Transform has not been asssigned");
        }
    }

    private void FixedUpdate()                                              // every update
    {
        grounded = false;                                                   // set grounded to false

        if (this.transform.position.y < -5 && !bIsDead)                     // If the player is out of bounds, and is not dead
            bIsDead = true;                                                 // Make them dead.
        if (this.rb2D.velocity.y > 20f)                                     // If the player is moving too fast on the Y axis
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, 10f);              // Slow them down
        }
        if (bHasBeenHurtRecently)                                           // If the player has been hurt recently
        {
            TimeSinceLastPain += Time.deltaTime;                            // Keep track of how long its been.
            if (TimeSinceLastPain >= PainDelay)                             // If it has been long enough
            {
                bHasBeenHurtRecently = false;                               // take away their damage immunity
                TimeSinceLastPain = 0f;                                     // stop keeping track of time
            }
        }
        this.GetComponent<SpriteRenderer>().color = new Color(1, (Health / StartHealth), (Health / StartHealth)); // Determine sprites color based on remaining health
        if (groundCheck)                                                    // If groundcheck exists
        {
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, GroundedRadius, groundLayers); // set the colliders positions
            for (int i = 0; i < colliders.Length; i++)                     // For each collider
            {
                if (colliders[i].gameObject != gameObject)                 // If the collider is not my game object
                {
                    grounded = true;                                       // We are on the ground
                    TimeSinceFirstJump += Time.deltaTime;                  // Keep track of time since first jump
                    if (TimeSinceFirstJump > 0.1f)                         // If the time since first jump is too long ago,
                    {       
                        CurrentJumpCount = 0;                              // Reset jump count
                        Debug.Log("RESET JUMP");
                        TimeSinceFirstJump = 0;                            // stop keeping track of time
                    }

                }
            }
        }

        anim.SetBool(GroundParam, grounded);                               // set animation
    }
    public void Move(float move, bool jump)                                // move function, takes in move value and jump bool
    {
        if (grounded || airControl)                                        // if grounded or air control
        {
            anim.SetFloat(SpeedParam, Mathf.Abs(move));                    // set the anim
            if (Mathf.Abs(impactForce) > .1)                               // if impact force is non-negligable 
            {
                rb2D.velocity = new Vector2(maxSpeed * impactForce, rb2D.velocity.y); // set the velocity of the player to match the impact
                if (impactForce > 0)                                       // if the impact force is larger than 0 
                    impactForce -= Time.deltaTime;                         // lower the force based on how much time has passed
                else                                                       // otherwise if impact force is lower than 0
                    impactForce += Time.deltaTime;                         // raise the force towards 0
            }
            else                                                           // Otherwise, impact force is negligable. Give player control
                if(bTimeIsSlow)
                    rb2D.velocity = new Vector2((move * maxSpeed), rb2D.velocity.y);
                else
                    rb2D.velocity = new Vector2(move * maxSpeed, rb2D.velocity.y);
            //Debug.Log(rb2D.velocity);

            if (move > 0 && !facingRight)                                  // if player is moving, and is not facing the right direction
            {
                Flip();                                                    // flip the direction
            }
            else if (move < 0 && facingRight)
            {
                Flip();
            }
        }
        // if the player is grounded, or their jump count is less than the maximum, AND we have been told to jump AND there is a ground parameter or the jump count is less than maximum
        if ((grounded || CurrentJumpCount < MaxJumpCount) && jump && (anim.GetBool(GroundParam) || CurrentJumpCount < MaxJumpCount))
        {
            grounded = false;                                               // player is no longer grounded
            CurrentJumpCount++;                                             // increment jump count
            Debug.Log(CurrentJumpCount);    
            anim.SetBool(GroundParam, false);                               // set the animation to jumping
            rb2D.AddForce(new Vector2(0f, jumpForce));                      // add force to the jump
        }
    }
    private void Flip()                                                     // flip function
    {
        facingRight = !facingRight;                                         // toggle the boolean

        Vector3 theScale = transform.localScale;                            // flip the scale
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    public void GetHurt(int Damage)                                         // Take damage function
    {
        if (!bHasBeenHurtRecently)                                          // if the player hasnt been hurt recently
        {
            Health = Health - Damage;                                       // Remove the damage from their health
            bHasBeenHurtRecently = true;                                    // note the player has taken recent damage
            if (Health <= 0)                                                // if the player is out of health
            {
                bIsDead = true;                                             // they are dead
            }
        }
    }

    public void SlowTime(float Speed)                                       // Slow time method
    {
        if (bTimeIsSlow)                                                    // if Time Is already slow
        {

            Time.timeScale = 1;                                             // Return Timescale to normal
            bTimeIsSlow = false;
        }

        else                                                                // otherwise
        {
            
            Time.timeScale = Speed;                                   // set world speed to determined value
            bTimeIsSlow = true;
        }
    }
}
