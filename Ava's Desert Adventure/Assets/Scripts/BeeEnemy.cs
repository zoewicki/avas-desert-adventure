﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeEnemy : Enemy {

    public override void Move()
    {
        if (!bShouldRetreat)
        {

            MyRigidbody.velocity = new Vector2(MoveDirection() * MoveSpeed, MyRigidbody.velocity.y);
            if (Mathf.Abs(transform.position.x - Player.transform.position.x) < 2)
            {
                MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, UpOrDown() * MoveSpeed);
            }
            else if (transform.position.y - Player.transform.position.y < 2)
                MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, 1 * MoveSpeed);
        }
        else
        {
            MyRigidbody.velocity = new Vector2(-MoveDirection() * RetreatSpeed, MyRigidbody.velocity.y);
            if (!(transform.position.y - Player.transform.position.y > 4))
            {
                MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, 1 * MoveSpeed);
            }
        }
            
    }
    private int UpOrDown()
    {
        if ((Player.transform.position.y - transform.position.y) < -0.1)
            return -1;
        else if ((Player.transform.position.y - transform.position.y) > 0.1)
            return 1;
        else
            return 0;
    }
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.collider == Player.GetComponent<Collider2D>())
        {
            bShouldRetreat = true;
            TimeSinceLastRetreat = 0f;
        }
    }
    public override void Update()
    {
        base.Update();
        if (bShouldRetreat)
        {
            TimeSinceLastRetreat += Time.deltaTime;
            if (TimeSinceLastRetreat >= RetreatTime)
            {
                bShouldRetreat = false;
                TimeSinceLastRetreat = 0f;
            }
        }
    }
}
