﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    //
    // HEALTH RELATED VARIABLES
    //
    [HideInInspector]
    public bool bIsDead = false;                    //False if the enemy is alive
    public float Health = 3f;                       //Number of health the enemy has
    public float StartHealth = 3f;                  //Number of health the enemy starts with
    private bool bHasBeenHurtRecently = false;      //False if the enemy has not been hurt recently
    private float TimeSinceLastPain = 0.0f;
    public float PainDelay = 0.25f;                 //How long before the enemy can be hurt again
    public float Damage = 1f;
    public bool bIsBoss = false;
    public bool Win = false;

    //
    // MOVEMENT RELATED VARIABLES
    //
    public float MoveSpeed = 3f;
    [HideInInspector]
    public AvaPawn Player;
    [HideInInspector]
    public Collider2D MyCollider;
    [HideInInspector]
    public Rigidbody2D MyRigidbody;
    [HideInInspector]
    public bool bShouldRetreat = false;

    public float RetreatTime = 1f;
    [HideInInspector]
    public float TimeSinceLastRetreat = 0f;
    public float RetreatSpeed = 5f;
    [HideInInspector]
    public SpriteRenderer MyRender;
    void Start () {
        Player = FindObjectOfType<AvaPawn>();
        MyCollider = GetComponent<Collider2D>();
        MyRigidbody = GetComponent<Rigidbody2D>();
        MyRender = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	public virtual void Update () {
        if(Player != null)
            if (!Player.bIsDead)
            {
                if (Mathf.Abs(Player.transform.position.x - transform.position.x) < 15)
                    Move();   
            }
        if(bIsDead || transform.position.y < -5)
        {
            if (bIsBoss)
            {
                Win = true;
                Debug.Log("You Win!");
            }
            if(!bIsBoss)
                Destroy(gameObject);
        }
        if (bHasBeenHurtRecently)
        {
            TimeSinceLastPain += TimeSinceLastPain + Time.deltaTime;
            if (TimeSinceLastPain >= PainDelay)
            {
                bHasBeenHurtRecently = false;
                TimeSinceLastPain = 0f;
            }
        }

        MyRender.color = new Color(1, Health / StartHealth, Health / StartHealth);
    }

    public virtual void Move()
    {
        MyRigidbody.velocity = new Vector2(MoveDirection() * MoveSpeed, MyRigidbody.velocity.y);
    }

    public int MoveDirection()
    {
        if ((Player.transform.position.x - transform.position.x) < -0.1)
            return -1;
        else if ((Player.transform.position.x - transform.position.x) > 0.1)
            return 1;
        else
            return 0;
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == Player.GetComponent<Collider2D>())
        {
            if (Player.transform.position.y - Player.GetComponent<Collider2D>().bounds.extents.y > transform.position.y + (MyCollider.bounds.extents.y / 1.1f))
            {
                GetHurt(0);
            }
            else
            {
                Player.GetHurt(Damage);
            }
        }
    }

    public virtual void GetHurt(float Damage)                                         // Take damage function
    {
        if (!bHasBeenHurtRecently)                                          // if the player hasnt been hurt recently
        {
            bShouldRetreat = true;
            Health = Health - Damage;                                       // Remove the damage from their health
            if(Damage > 0)
                bHasBeenHurtRecently = true;                                    // note the player has taken recent damage
            if (Health <= 0)                                                // if the player is out of health
            {
                bIsDead = true;                                             // they are dead
            }
        }
    }
}
