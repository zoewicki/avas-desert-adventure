﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionEnemy : Enemy {

    private int RetreatDirection = 1;

    public override void Move()
    {
        if (!bShouldRetreat)
            MyRigidbody.velocity = new Vector2(MoveDirection() * MoveSpeed, MyRigidbody.velocity.y);
        else
        {
            if (MoveDirection() == 0)
                MyRigidbody.velocity = new Vector2(RetreatDirection * RetreatSpeed, MyRigidbody.velocity.y);
            else
                MyRigidbody.velocity = new Vector2(-MoveDirection() * RetreatSpeed, MyRigidbody.velocity.y);
        }
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.collider == Player.GetComponent<Collider2D>())
        {
            bShouldRetreat = true;
            TimeSinceLastRetreat = 0f;
        }
    }
    public override void Update()
    {
        base.Update();
        if (bShouldRetreat)
        {
            TimeSinceLastRetreat += Time.deltaTime;
            if(TimeSinceLastRetreat>= RetreatTime)
            {
                bShouldRetreat = false;
                TimeSinceLastRetreat = 0f;
            }
        }
    }
}
