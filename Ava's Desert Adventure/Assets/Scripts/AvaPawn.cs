﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AvaPawn : MonoBehaviour {

    // Use this for initialization


    //
    // HEALTH RELATED VARIABLES
    //
    public bool bIsDead = false;                    //False if the player is alive
    public float Health = 3f;                       //Number of health the player has
    public float StartHealth = 3f;                  //Number of health the player starts with
    public Slider healthSlider;                     // Reference to the UI's health bar.
    private bool bHasBeenHurtRecently = false;      //False if the player has not been hurt recently
    private float TimeSinceLastPain = 0.0f;
    public float PainDelay = 0.25f;                 //How long before the player can be hurt again
    public SpriteRenderer MyRender;
    public float DeathDelay = 3;


    //
    // TIME RELATED VARIABLES
    //
    public float SlowPrecent = .33f;                // The speed at which the game will run at when slowing down
    public bool bTimeIsSlow = false;                // false if time is normal speed



    //
    // COLLISION RELATED VARIABLES
    //
    private float distToGround;                     // The distance to the ground
    private Collider2D MyCollider;
    private Rigidbody2D MyRigidbody;

    public LayerMask LayersToCheck;                 // The layers to check for collision

    //
    // RESOURCE RELATED VARIABLES 
    //
    private int WaterCount;
    public float WaterDuration = .5f;
    private float TimeSinceLastWaterConsumed = 0f;

    //
    // MOVEMENT RELATED VARIABLES 
    //
    public bool bAirControl = false;
    public float MoveSpeed = 5f;
    public float JumpForce = 400f;
    public float AdjustedJumpForce;
    float AdjustedMoveSpeed;
    private Animator Anim;
    private const string MovingAnim = "Speed";
    private const string JumpingAnim = "Ground";

    //
    // ATTACK RELATED VARIABLES
    //

    public float AttackRange = 1;
    public float AttackSize = 1;
    public float AttackDamage = 1;
    public LayerMask EnemyLayer;
    public float AttackDelay = 0.75f;
    private float TimeSinceLastAttack = 0f;
    private bool bHasAttackedRecently = false;
    private const string AttackAnim = "AvaSwing";
    private const string HurtAnim = "AvaHurt";
    private AudioSource MySound;
    public AudioClip HitSound;
    public AudioClip HurtSound;
    public AudioClip WaterSound;
    //
    // TEXT RELATED VARIABLES 
    //
    public Text countText;
    public Text winText;

    void Start () {
        MyCollider = GetComponent<Collider2D>();
        MyRigidbody = GetComponent<Rigidbody2D>();
        Anim = GetComponent<Animator>();
        MyRender = GetComponent<SpriteRenderer>();
        distToGround = MyCollider.bounds.extents.y;
        MySound = GetComponent<AudioSource>();

        // Initialize count to 0
        WaterCount = 0;

        // Call SetCountTetx
        SetCountText();
	}

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !bHasAttackedRecently)
        {
            Attack();
        }
        else if(bHasAttackedRecently)
        {
            TimeSinceLastAttack += Time.deltaTime / Time.timeScale;
            if (TimeSinceLastAttack >= AttackDelay)
            {
                bHasAttackedRecently = false;
                TimeSinceLastAttack = 0;
            }
        }
        if (Input.GetAxis("Jump") >0 && bIsGrounded())
        {
            if (bTimeIsSlow)
                AdjustedJumpForce = JumpForce / SlowPrecent;
            else
                AdjustedJumpForce = JumpForce;
            ApplyJump(AdjustedJumpForce);
            Anim.SetBool(JumpingAnim, false);
        }
        else
            Anim.SetBool(JumpingAnim, bIsGrounded());
        if (Input.GetKeyDown(KeyCode.X))
        {
            SlowTime(SlowPrecent);
        }
        //if(Input.GetAxis("Horizontal") != 0)
        //{
            Move();
        //}
        if (this.transform.position.y < -5)
            GetHurt(100);
        if (bIsDead)
        {
            if (bTimeIsSlow)
                SlowTime(SlowPrecent);
           
            StartCoroutine(DeathLevelAfterDelay(DeathDelay));
            MoveSpeed = 0;
            gameObject.GetComponent<Renderer>().enabled = false;
        }
        if(bTimeIsSlow)
        {
            if(!bIsGrounded())
                MyRigidbody.AddForce(Physics2D.gravity * (((MyRigidbody.mass*MyRigidbody.gravityScale) / SlowPrecent)));
            
            TimeSinceLastWaterConsumed += Time.deltaTime;
            if (TimeSinceLastWaterConsumed >= WaterDuration)
            {
                WaterCount--;
                SetCountText();
                TimeSinceLastWaterConsumed = 0f;
            }
            if (WaterCount <= 0)
                SlowTime(SlowPrecent);
        }
        if(bHasBeenHurtRecently)
        {
            MyRender.color = new Color(1, Health / StartHealth, Health / StartHealth);
            TimeSinceLastPain += Time.deltaTime;
            if (TimeSinceLastPain >= PainDelay)
            {
                bHasBeenHurtRecently = false;
                TimeSinceLastPain = 0f;
            }
        }
        else
            MyRender.color = new Color(1,1,1);
    }

    IEnumerator DeathLevelAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);

        SceneManager.LoadScene("LoseScreen");

    }
    public void Attack()
    {
        Debug.Log("Attacking. bHasAttackedRecently =" + bHasAttackedRecently);
        Anim.Play(AttackAnim);
        Collider2D EnemyHit;
        EnemyHit = Physics2D.CircleCast(transform.position, AttackSize, transform.forward, AttackRange, EnemyLayer).collider;
        bHasAttackedRecently = true;
        if (EnemyHit)
        {
            EnemyHit.GetComponent<Enemy>().GetHurt(AttackDamage);
            MySound.pitch = Random.Range(.5f, 1.5f);
            MySound.PlayOneShot(HitSound);
            
        }
    }
    public bool bIsGrounded()                                               // Check if player is on the ground and return boolean
    {

        Collider2D LandedOn;

        LandedOn = (Physics2D.Raycast(transform.position, Vector2.down, distToGround, LayersToCheck).collider);
        if (LandedOn == null)
            LandedOn = Physics2D.Raycast(new Vector2(transform.position.x + MyCollider.bounds.extents.x -0.1f, transform.position.y), Vector2.down, distToGround + 1.1f, LayersToCheck).collider;
        if(LandedOn == null)
            LandedOn = Physics2D.Raycast(new Vector2(transform.position.x - MyCollider.bounds.extents.x +0.1f, transform.position.y), Vector2.down, distToGround + 1.1f, LayersToCheck).collider; // Raycast down on edges of pawn and center

        //LandedOn.gameObject.GetComponent<>();         // Check if enemy, and hurt them.

        return (LandedOn != null);
    }

    public void Move()
    {
        if (bIsGrounded() || bAirControl)
        {
            Anim.SetFloat(MovingAnim, Mathf.Abs(Input.GetAxis("Horizontal")));
            if (bTimeIsSlow)
            {
                Anim.SetFloat("WalkSpeedMult", 1 / SlowPrecent);
                AdjustedMoveSpeed = MoveSpeed; // SlowPrecent;
               /* if (Input.GetAxis("Horizontal") > 0.05)
                    MyRigidbody.velocity = new Vector2((1 * AdjustedMoveSpeed) / SlowPrecent, MyRigidbody.velocity.y);
                else if(Input.GetAxis("Horizontal") < -0.05)
                    MyRigidbody.velocity = new Vector2((-1 * AdjustedMoveSpeed) / SlowPrecent, MyRigidbody.velocity.y);
                else*/
                    MyRigidbody.velocity = new Vector2((Input.GetAxis("Horizontal") * AdjustedMoveSpeed) / SlowPrecent, MyRigidbody.velocity.y);
            }
            else
            {
                Anim.SetFloat("WalkSpeedMult", 1);
                AdjustedMoveSpeed = MoveSpeed;
                MyRigidbody.velocity = new Vector2(Input.GetAxis("Horizontal") * AdjustedMoveSpeed, MyRigidbody.velocity.y);
            }
            Flip(Input.GetAxis("Horizontal"));
            
        }
    }
    public void Flip(float Direction)
    {
        Vector3 MyScale = transform.localScale;
        if (Direction < 0 && MyScale.x >0)
        {
            MyScale.x *= -1;
            transform.localScale = MyScale;
        }
        else if (Direction > 0 && MyScale.x <0)
        {
            MyScale.x *= -1;
            transform.localScale = MyScale;
        }
    }
    public void ApplyJump(float JumpForce)
    {
        MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, JumpForce);
    }

    public void GetHurt(float Damage)                                       // Take damage function
    {
        if (!bHasBeenHurtRecently)                                          // if the player hasnt been hurt recently
        {
            
            Health = Health - Damage;                                       // Remove the damage from their health
            bHasBeenHurtRecently = true;                                    // note the player has taken recent damage
            Anim.Play(HurtAnim);
            if (Health <= 0)                                                // if the player is out of health
            {
                bIsDead = true;                                             // they are dead
            }
            healthSlider.value = Health;                                    // changes health slider value when hurt
            MySound.pitch = Random.Range(1f, 1.5f);
            MySound.PlayOneShot(HurtSound);
        }
    }
    public void SlowTime(float Speed)                                       // Slow time method
    {
        if (bTimeIsSlow)                                                    // if Time Is already slow
        {
            Time.timeScale = 1;                                             // Return Timescale to normal
            bTimeIsSlow = false;
            MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, MyRigidbody.velocity.y * SlowPrecent);
        }

        else                                                                // otherwise
        {
            if (WaterCount > 0)
            {
                Time.timeScale = Speed;                                   // set world speed to determined value
                bTimeIsSlow = true;
                MyRigidbody.velocity = new Vector2(MyRigidbody.velocity.x, MyRigidbody.velocity.y / SlowPrecent);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("TRIGGERENTER");
        if (other.gameObject.CompareTag("water"))
            other.gameObject.SetActive(false);

        WaterCount = WaterCount + 1;
        SetCountText();
        MySound.pitch = 1;
        MySound.PlayOneShot(WaterSound, 5);

    }

    void SetCountText()
    {
        countText.text = "Droplets: " + WaterCount.ToString();
    }

}
