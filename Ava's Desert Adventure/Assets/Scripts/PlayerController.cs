﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerPawn))]
public class PlayerController : MonoBehaviour
{
    private PlayerPawn character;
    private bool jump;

    private void Awake()
    {
        character = GetComponent<PlayerPawn>();
    }

    private void Update()
    {
        if (!jump)
        {
            jump = Input.GetButtonDown("Jump");
        }
        if (Input.GetButtonDown("Slow"))
        {
            character.SlowTime(character.SlowPrecent);
        }
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        character.Move(h, jump);
        jump = false;
    }
}