﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform player;
    public AvaPawn Ava;
    public Vector3 offset;
    public Vector2 minimumBoundary;
    public Vector2 maximumBoundary;
    public AudioSource Music;


    void Update()
    {
        if (player)
        {
            transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, offset.z); // Camera follows the player with specified offset position

            transform.position = new Vector3
            (
                Mathf.Clamp(transform.position.x, minimumBoundary.x, maximumBoundary.x),
                Mathf.Clamp(transform.position.y, minimumBoundary.y, maximumBoundary.y),
                transform.position.z
            );
        }
        if (Ava.bTimeIsSlow)
        {
            Music.pitch = Ava.SlowPrecent*2;
        }
        else
        {
            Music.pitch = 1;
        }
    }

}
